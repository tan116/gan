create table book
(
    isdn  varchar(255)   null,
    name  varchar(255)   null,
    price decimal(10, 2) null
);