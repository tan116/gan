import org.example.com.cn.Business.SaleService;
import org.example.com.cn.DAO.BookDAOImpl;
import org.example.com.cn.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@ContextConfiguration("classpath*:database-context.xml")
public class BeanTest extends AbstractTestNGSpringContextTests {
    @Autowired
    Book book;
    @Autowired
    SaleService service;
    @Test
    public void beantest(){
        book.setName("java ee");
        assertTrue(book!=null);
    }

   @Test
   //增加数据
   public void insert(){
        book.setIsdn("666");
        book.setName("web");
        book.setPrice(100);
        service.insert(book);
    }
    @Test
    //修改数据
    public void modify(){
        book.setIsdn("888");
        book.setName("jhk");
        book.setPrice(1000);
        service.modify(book);
    }
    @Test
    //删除数据
    public void delete(){
        book.setIsdn("888");
        service.delete(book);
    }
    @Test
    public void select(){
        service.queryAll();
    }
    @Test
    public void findUserByIdTest() {
        //初始化Spring容器，加载applicationContext.xml文件
        ApplicationContext ctx= new ClassPathXmlApplicationContext("database-context.xml");
        //通过容器获取UserDAOImpl的实例
        BookDAOImpl BookDAOImpl=(BookDAOImpl) ctx.getBean("BookDAOImpl");
        //执行findUserById方法，获取User对象
        Book book=BookDAOImpl.findUserById("666");
        System.out.println(book);
    }

    @Test
   public void onSale(){
        service.onSale(0.5f);
    }

}
