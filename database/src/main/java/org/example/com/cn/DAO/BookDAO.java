package org.example.com.cn.DAO;

import org.apache.ibatis.annotations.Mapper;
import org.example.com.cn.Model.Book;

import java.util.List;
@Mapper
public interface BookDAO {
    void insertBook(Book book);//增加数据的方法
    void modifyBook(Book book);//修改数据的方法
    void deleteBook(Book book);//删除数据的方法
    List<Book> queryAll();//这个List里面只能存放Book类型的数据，这也是一个方法，返回值类型是list的



    void insertAll(List<Book> bookList);//这个方法是
    void deleteAll(List<Book> bookList);
    void deleteById(String id);//定义一个方法实现通过id进行删除操作
    Book queryOneBook(Book book);//定义一个返回类型是Book的方法
    Book queryOneBookById(String id);//应该是通过id进行查询的方法
}
