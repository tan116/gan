package org.example.com.cn.DAO;

import org.example.com.cn.Model.Book;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
@Repository(value="bookDaoImpl")
public class BookDAOImpl extends AbstractBookDAO {
    @Override
    public void insertBook(Book book) {
        super.insertBook(book);
        String INSERT_BOOK="INSERT INTO book(isdn,name,price) VALUES(?,?,?)";
        Object[] args={book.getIsdn(),book.getName(),book.getPrice()};
        jdbcTemplate.update(INSERT_BOOK,args);

    }

    @Override
    public void modifyBook(Book book) {
        super.modifyBook(book);
        String MODIFY_BOOK="update book set price=?";
        Object[] args={book.getPrice()};
        jdbcTemplate.update(MODIFY_BOOK,args);
    }
    public void deleteBook(Book book) {
        super.deleteBook(book);
        String DELETE_BOOK="delete from book where isdn=?";
        Object[] args={book.getPrice()};
        jdbcTemplate.update(DELETE_BOOK,args);
    }

    public Book findUserById(String isdn) {
        // 定义单个查询的SQL语句
        String sql = "select * from book where isdn=?";
        // 创建一个新的BeanPropertyRowMapper对象，将结果集通过Java的反射机制映射到Java对象中
        RowMapper<Book> rowMapper = new BeanPropertyRowMapper<Book>(Book.class);
        // 将id绑定到SQL语句中，并通过RowMapper返回一个Object类型的对象
        return this.jdbcTemplate.queryForObject(sql, rowMapper, isdn);
    }

    @Override
    public List<Book> queryAll() {
        String QUERY_ALL_BOOK="select*from book";//定义一个基础的sql查询语句
        RowMapper<Book> rowMapper=new BeanPropertyRowMapper<Book>(Book.class);
        books=jdbcTemplate.query(QUERY_ALL_BOOK,rowMapper);
        return books;
    }
}
