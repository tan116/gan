package org.example.com.cn.Business;

import org.example.com.cn.DAO.BookDAO;
import org.example.com.cn.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class SaleService {
    BookDAO bookDAO;

    @Autowired
    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }
    public void insert(Book book){
        bookDAO.insertBook(book);
    }
    public void modify(Book book){
        bookDAO.modifyBook(book);
    }
    public void delete(Book book){
        bookDAO.deleteBook(book);
    }
    public void queryAll(){
        bookDAO.queryAll();

    }


    @Transactional
    public void onSale(float sale){
        List<Book> bookList=bookDAO.queryAll();
        for(Book b:bookList){
            b.setPrice(b.getPrice()*sale);
            bookDAO.modifyBook(b);
        }
    }
}
