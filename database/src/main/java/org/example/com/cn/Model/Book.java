package org.example.com.cn.Model;

import org.springframework.stereotype.Component;

@Component
public class Book {
    private String name;
    private String isdn;
    private float price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", isdn='" + isdn + '\'' +
                ", price=" + price +
                '}';
    }
}
